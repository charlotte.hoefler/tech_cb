import pandas as pd
import sys


references=pd.read_csv('data/'+sys.argv[1], delimiter=';', dtype='a')
retailer=pd.read_csv('data/'+sys.argv[2], delimiter=';', dtype='a')

def question1(references,retailer):
    references_id=references['reference_id'].dropna()
    retailer_ean=retailer['EAN'].dropna()
    a=~retailer_ean.isin(references_id)
    retailer_ean=retailer_ean[a.values]
    return retailer_ean

def question2(retailer, references):
    retailer_needed=retailer[['Libellé  Sous-Famille ','EAN','Article Libellé Long','Date déréf.']]
    aisle_tracked=references[['allee','reference_id']].sort_values(by=['reference_id'])


    references_id=references['reference_id'].dropna()
    reatiler_list=retailer[['EAN','Article Libellé Long','Libellé  Sous-Famille ']]
    a=reatiler_list['EAN'].isin(references_id)
    final = reatiler_list[a.values].sort_values(by=['EAN'])


    final.set_index('EAN', inplace=True)
    aisle_tracked.set_index('reference_id', inplace=True)
    #Problem with the join() method (The joint isn't registered in the variable final). Must reconvert to csv and reopen it to be able to have the data.
    final.join(aisle_tracked).sort_values(by=['allee']).drop_duplicates(subset=['Libellé  Sous-Famille ']).to_csv('listetracked.csv')
    final=pd.read_csv('listetracked.csv')
    final=final[['Libellé  Sous-Famille ','allee']]

    na_free=retailer_needed.dropna()
    only_na = retailer_needed[~retailer_needed.index.isin(na_free.index)]
    final_retailer=only_na.dropna(thresh=3)

    b=final_retailer['Libellé  Sous-Famille '].isin(final['Libellé  Sous-Famille '])
    products_in_aisle_tracked=final_retailer[b.values]
    not_already_tracked_products=question1(references,retailer).tolist()
    c=products_in_aisle_tracked['EAN'].isin(not_already_tracked_products)
    answer2=products_in_aisle_tracked[c.values]

    answer_bonus=answer2[['EAN','Libellé  Sous-Famille ']]
    answer_bonus.merge(final).to_csv('Answer_bonus.csv')


    answer2=answer2[['EAN','Article Libellé Long']]
    answer2.to_csv('Answer2.csv')
    return answer2








if __name__ == '__main__':



    answer1=question1(references,retailer)
    print("The total number of references not tracked in the app but present in the shop assortment is:",answer1.shape[0])


    answer2=question2(retailer,references)
    print("The answer 2 has been exported in the file Answer2.csv in the current repository")

    print("The total size of the list of relevant but not tracked products is:", answer2.shape[0])

    print("The bonus answer has been exported in the file Answer_bonus.csv in the current repository")
