# TECH_CB

- In this Repo, you will find the python program, **one directory to put the data**, and one directory containing SQL queries.

- To run the python program with your one files in the data directory, please run : 

`python dataexplorer.py  "<references tracked.csv>" "<retailer extract.csv>"`

For example, with the furnished set of data, we can run with :

`python dataexplorer.py "references initialized in shop.csv" "retailer extract.csv"`

- **Please note that some answers (answer 2 and answer bonus) are exported in .csv files in the python program directory.**

- Answerbonus (in which aisle should not referenced but relevant products go) doesn't provide logicale answers for all products. The method applied was : 
If this already tracked product from this subfamily is in this aisle, then we can put this not already tracked product from the same subfamily in the same aisle.

_The SQL query 4 doesn't work._
