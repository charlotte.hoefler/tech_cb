
/*Challenge 4 : Not working.*/
SELECT "retailer extract"."Libellé  Sous-Famille",
       SUM(CASE WHEN date("references initialized in shop".expiry_date)<date('2021-10-20') THEN 1 ELSE 0 END) as Expired,
       SUM(CASE WHEN date("references initialized in shop".expiry_date) BETWEEN date('2021-10-20') AND date('2021-10-24') THEN 1 ELSE 0 END) as Not_Safe,
       SUM(CASE WHEN date("references initialized in shop".expiry_date)>date('2021-10-25') THEN 1 ELSE 0 END) as Safe
FROM "references initialized in shop","retailer extract"
WHERE "references initialized in shop"."reference_id" IN(SELECT "retailer extract"."ean"
FROM "retailer extract"
WHERE "retailer extract"."Libellé  Groupe de Famille " IN ('CHARCUTERIE'))
GROUP BY "retailer extract"."Libellé  Sous-Famille"
ORDER BY Expired DESC;
