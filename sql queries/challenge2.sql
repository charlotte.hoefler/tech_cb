/* Challenge 2*/
SELECT SUM(CASE WHEN "references initialized in shop".expiry_date<date('2021-10-20') THEN 1 ELSE 0 END) as Expired,
       SUM(CASE WHEN "references initialized in shop".expiry_date BETWEEN date('2021-10-20') AND date('2021-10-24') THEN 1 ELSE 0 END) as Not_Safe,
       SUM(CASE WHEN "references initialized in shop".expiry_date>date('2021-10-25') THEN 1 ELSE 0 END) as Safe
FROM "references initialized in shop";
