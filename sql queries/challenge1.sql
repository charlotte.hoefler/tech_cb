/* Challenge 1 : result match with the python version */
SELECT COUNT("retailer extract".ean)
FROM "retailer extract"
WHERE "retailer extract".ean NOT IN(SELECT "references initialized in shop".reference_id FROM "references initialized in shop");
